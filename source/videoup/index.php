﻿<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
<link rel="stylesheet" href="js/jquery.plupload.queue/css/jquery.plupload.queue.css" type="text/css" media="screen" />
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<!-- production -->
<script type="text/javascript" src="js/plupload.full.min.js"></script>
<script type="text/javascript" src="js/jquery.plupload.queue/jquery.plupload.queue.js"></script>
</head>
<body style="font: 13px Verdana; background: #eee; color: #333">
<?php
$basedir="../../dataset/yt/";
$allloc=array();

if ($handle = opendir($basedir)) {
    /* This is the correct way to loop over the directory. */
    while (false !== ($entry = readdir($handle))) {
        if ((is_dir($basedir.$entry)) && ($entry!=".") && ($entry != ".."))
        	$allloc[]=$entry;
    }
    closedir($handle);
}

//get one location
if (!isset($_GET['loc']))
{
	echo("<h2>Válassz helyszínt</h2>");
	foreach($allloc as $a1)
	{
		echo("<a href='?loc=$a1'>$a1</a><br/>");
	}
	die();
}else{
	echo("<h2>Letöltés: ".$_GET['loc']."</h2>");

	if ($handle = opendir($basedir.$_GET['loc'])) {
    /* This is the correct way to loop over the directory. */
    while (false !== ($entry = readdir($handle))) {
        if ((!is_dir($basedir.$entry)) && ($entry!=".") && ($entry != ".."))
        	{
        		echo("<a href='".$basedir.$_GET['loc']."/".$entry."'>$entry</a><br/>");
        	}
    }
    closedir($handle);
}

}
?>
<h1>Darabolást leíró txt feltöltése</h1>
<form method="post" action="dump.php?loc=<?=$_GET['loc']?>" enctype="multipart/form-data">	
	<div id="uploader">
		<p>Your browser doesn't have Flash, Silverlight or HTML5 support.</p>
	</div>
</form>

<script type="text/javascript">
$(function() {
	
	// Setup html5 version
	$("#uploader").pluploadQueue({
		// General settings
		runtimes : 'html5,flash,silverlight,html4',
		url : 'upload.php?loc=<?=$_GET['loc']?>',
		chunk_size: '1mb',
		rename : true,
		dragdrop: true,
		
		filters : {
			// Maximum file size
			max_file_size : '100mb',
			// Specify what files to browse for
			mime_types: [
				{title : "txt files", extensions : "txt"}
			]
		},

		flash_swf_url : '../../js/Moxie.swf',
		silverlight_xap_url : '../../js/Moxie.xap'
	});

});
</script>

</body>
</html>
