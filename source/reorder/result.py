import csv
import numpy as np
from collections import defaultdict
import copy

with open('output.csv', 'rb') as csvfile:
	csvreader = csv.reader(csvfile, delimiter=';')
	csvreader.next()
	clust = defaultdict()
	for row in csvreader:
		a = np.array(row[0:-7], dtype=float)
		s = np.array(row[-5], dtype=str)
		c = str(row[-3])
		if clust.has_key(c):
			m = [np.vstack((a,clust[c][0])), np.vstack((s,clust[c][1]))]
			clust[c] = m
		else:
			clust[c] = [a, s]
			
	comp = clust.copy()
	for r in comp.values():
		r[0] = np.linalg.norm(np.mean(r[0],0))
		#mn = np.mean(r[0],0)
		#tt = []
		#for k in r[0]:
		#	tt.append(np.linalg.norm(np.mean(k,0)))
		#r = [mn, np.array(tt), r[1]]

#	for r in comp.values():
#		print(r[0])
		#order = np.argsort(r[1])
		#for i in range(len(order)):
		#	print(r[2][i][0])
#		r[0] = np.sort(r[0],order=order)
#		r[1] = np.sort(r[1],order=order)
#		print(r[0])
#		print(r[1])
		#print(r[0].shape)
		#print(r[1].squeeze().shape)
		#print(np.vstack((r[0],r[1].squeeze())).T)
		#for rr in sorted(r):
		#	print(rr)
			#print(str(rr[0]) + rr[1])

	mm = max(comp.values())[0]
	scomp = sorted(comp.values())
	for r in scomp:
		r[0] = mm - r[0]
		r[1] = np.sort(r[1],0)

	with open('final.csv', 'w') as newcsv:
		tt = True
		while tt:
			tt = False
			for r in scomp:
				#if r[0]>0 and r[1].shape[0] > 0:
				if r[1].shape[0] > 0:
					newcsv.write(r[1][0][0] + "\n")
					r[1] = np.delete(r[1],0, 0)
					r[0] -= 1
					tt = True

		coll = []
		for r in scomp:
				for dd in r[1]:
					coll.append(dd[0])
#					newcsv.write(dd[0] + "\n")
		for s in sorted(coll):
			newcsv.write(s + "\n")

