from face_relevance_organizer import FaceRelevanceOrganizer
import sys

def main():
	if len(sys.argv) < 2:
		print("missing csv file parameter")
		sys.exit(-1)

	csv = sys.argv[1]
	orgzr = FaceRelevanceOrganizer(csv)
	print(orgzr.run())

if __name__ == "__main__":
	main()
