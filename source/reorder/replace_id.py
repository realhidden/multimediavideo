import re

d = [line.strip() for line in open('dictionary.txt')]
f = [line.strip() for line in open('SummerPalace.csv')]

with open('output.csv', 'w') as fl:
	for l in f:
		tt = l
		for r in d:
			s = r.split('-')
			orig = '-'.join(s[:3])
			tt = re.sub(orig, r, tt)
		fl.write(tt)
		fl.write("\n")
