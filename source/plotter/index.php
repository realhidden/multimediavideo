<style>
    h2,h3{margin:0;padding:0;}
    .chartblokk{float:left;}
</style>
<?php
$basedir = "../../dataset/yt/";
$allloc = array();

if ($handle = opendir($basedir)) {
    /* This is the correct way to loop over the directory. */
    while (false !== ($entry = readdir($handle))) {
        if ((is_dir($basedir . $entry)) && ($entry != ".") && ($entry != ".."))
            $allloc[] = $entry;
    }
    closedir($handle);
}

//get one location
if (!isset($_GET['loc'])) {
    ?>
    <form method="get">
        Helyszín:
        <select name="loc">
            <?php
            foreach ($allloc as $a1) {
                echo("<option>$a1</option>");
            }
            ?>
        </select>
        Statisztikák alapja:
        <select name="timebase">
            <option value="0">shotok</option>
            <option value="1">idő</option>
        </select>
        <input type="submit">
    </form>
    <?php
    die();
}

$short_to_longname=array();

function loadTwovaluedCsv($filename) {
    global $short_to_longname;
    
    if (!file_exists($filename))
        die("Unable to load: " . $filename);

    $ret = array();
    $lines = explode("\n", file_get_contents($filename));
    foreach ($lines as $l1) {
        $l1 = explode(" ", $l1);

        if ($l1[0] == "")
            continue;

        //filter result
        $original=$l1[0];
        
        $filt=$l1[0];
        $filt=explode("-",$filt);
        $filt=array_slice($filt,0,3);
        $filt=implode("-",$filt);
        $ret[$filt] = $l1[1];
        
        $short_to_longname[$filt]=$original;
    }
    ksort($ret);
    return $ret;
}

function convertToSeconds($timecode) {
    $fulltime = 0;
    $timecode = explode("_", $timecode);
    //hour
    $fulltime+=60 * 60 * $timecode[0];
    //minute
    $fulltime+=60 * $timecode[1];
    //seconds
    $fulltime+=$timecode[2];

    if (count($timecode) == 4)
        $fulltime+= (float) ("0." . $timecode[3]);

    return $fulltime;
}

function getTimediffFromShot($shot) {
    global $short_to_longname;
    
    if (!isset($short_to_longname[$shot]))
    {
        return 0;
    }
    $shotdetail = explode("-", $short_to_longname[$shot]);

    //grab start and end time from filename
    $starttime = "00_00_00_000";
    $endtime = "";

    if ($shotdetail[3] != "0") {
        $starttime = $shotdetail[3];
    }
    $endtime = $shotdetail[4];

    return convertToSeconds($endtime) - convertToSeconds($starttime);
}

/**
 * Calculate the relevance chart using the order, rGT
 * @param type $theorder
 * @param type $rGT
 */
function calculateRelevanceChart($theorder, $rGT, $use_shot_length = false) {
    $ret = array();
    $oldvalue = 0;
    $currtime = 0;
    
    foreach ($theorder as $shot) {
        if (trim($shot)=="")
            continue;
        //add the shot length (calculate time if needed)

        $timemod = 1;
        if ($use_shot_length == true) {
            $timemod = getTimediffFromShot($shot);
        }
        $currtime+=$timemod;

        $oldvalue+=$rGT[$shot] * $timemod;
        $ret[] = array($currtime, $oldvalue);
    }

    return $ret;
}

/**
 * Calculate the diversity chart using the order, dGT
 * @param type $theorder
 * @param type $dGT
 */
function calculateDiversityChart($theorder, $dGT, $use_shot_length = false) {
    $ret = array();
    $oldvalue = 0;
    $currtime = 0;
    $seenclusters = array();

    foreach ($theorder as $shot) {
        if (trim($shot)=="")
            continue;
        //add the shot length (calculate time if needed)

        $timemod = 1;
        if ($use_shot_length == true) {
            $timemod = getTimediffFromShot($shot);
        }
        $currtime+=$timemod;


        if (!in_array($dGT[$shot], $seenclusters)) {
            $oldvalue+=1;
            $seenclusters[] = $dGT[$shot];
        }

        $ret[] = array($currtime, $oldvalue);
    }

    return $ret;
}

//load annotation
$rGT = loadTwovaluedCsv("../../dataset/annotation/" . $_GET['loc'] . ".rGT.csv");
$dGT = loadTwovaluedCsv("../../dataset/annotation/" . $_GET['loc'] . ".dGT.csv");

$origorder = array_keys($rGT);

//grab the "other" ordering
$yourorder=@$_POST['yourorder'];
if (trim($yourorder)!="")
{
    $yourorder=explode("\r\n",$yourorder);
}else{
    $yourorder=$origorder;
}

$orig_relevance_plot = calculateRelevanceChart($origorder, $rGT, $_GET['timebase'] == 1);
$orig_diversity_plot = calculateDiversityChart($origorder, $dGT, $_GET['timebase'] == 1);

$your_relevance_plot = calculateRelevanceChart($yourorder, $rGT, $_GET['timebase'] == 1);
$your_diversity_plot = calculateDiversityChart($yourorder, $dGT, $_GET['timebase'] == 1);


$your_p1_plot=array();
$orig_p1_plot=array();

//f1 calculation
$d=array();
foreach($orig_diversity_plot as $onevalue)
{
    $d[$onevalue[0]]=$onevalue[1];
}

$maxd=$orig_diversity_plot[count($orig_diversity_plot)-1][1];
foreach($orig_relevance_plot as $onevalue)
{
    list($time,$r1)=$onevalue;
    
    $d1=$d[$time]/$maxd;
    $r1=$r1/$time;
    $orig_p1_plot[]=array($time,2*($r1*$d1)/($r1+$d1));
}

$d=array();
foreach($your_diversity_plot as $onevalue)
{
    $d[$onevalue[0]]=$onevalue[1];
}

$maxd=$your_diversity_plot[count($your_diversity_plot)-1][1];
foreach($your_relevance_plot as $onevalue)
{
    list($time,$r1)=$onevalue;
    $d1=$d[$time]/$maxd;
    $r1=$r1/$time;
    $your_p1_plot[]=array($time,2*($r1*$d1)/($r1+$d1));
}
?>
<h2><?php echo($_GET['loc'] . " - " . (($_GET['timebase'] == 1) ? "time based" : "shot based")); ?></h2>
<div class="chartblokk">
    <h3>P@N (relevant items)</h3>
    <div id="chart_relevance" style="width: 700px; height: 400px;"></div>
</div>
<div class="chartblokk">
    <h3>CR@N</h3>
    <div id="chart_diversity" style="width: 700px; height: 400px;"></div>
</div>
<div class="chartblokk">
    <h3>Relevant item % so far</h3>
    <div id="chart_relevance_sofar" style="width: 700px; height: 400px;"></div>
</div>
<div class="chartblokk">
    <h3>CR@N (normalized)</h3>
    <div id="chart_diversitynormal" style="width: 700px; height: 400px;"></div>
</div>
<div class="chartblokk">
    <h3>F1</h3>
    <div id="chart_f1" style="width: 700px; height: 400px;"></div>
</div>
<div class="chartblokk">
    <h3>Your ordering</h3>
    <form method="POST">
        <textarea name='yourorder' style="width: 600px; height: 300px;"><?=implode("\n",$yourorder)?></textarea><br/>
        <input type="submit">
    </form>
</div>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
    var orig_relevance =<?= json_encode($orig_relevance_plot) ?>;
    var orig_diversity =<?= json_encode($orig_diversity_plot) ?>;
    var your_relevance =<?= json_encode($your_relevance_plot) ?>;
    var your_diversity =<?= json_encode($your_diversity_plot) ?>;
    var orig_f1=<?= json_encode($orig_p1_plot) ?>;
    var your_f1=<?= json_encode($your_p1_plot) ?>;

    google.load("visualization", "1", {packages: ["corechart"]});
    google.setOnLoadCallback(drawChart);
               
    function drawChart() {
        //relevance
        var pureRelevance = new google.visualization.DataTable();
        pureRelevance.addColumn('number', 'X');
        pureRelevance.addColumn('number', 'Original');
        pureRelevance.addColumn('number', 'Given');

        var sofarRelevance = new google.visualization.DataTable();
        sofarRelevance.addColumn('number', 'X');
        sofarRelevance.addColumn('number', 'Original');
        sofarRelevance.addColumn('number', 'Given');

        

        for (var i = 0; i < orig_relevance.length; i++) {
            pureRelevance.addRow([orig_relevance[i][0], orig_relevance[i][1], null]);
            sofarRelevance.addRow([orig_relevance[i][0], orig_relevance[i][1] / orig_relevance[i][0] * 100, null]);
        }
        for (var i = 0; i < your_relevance.length; i++) {
            pureRelevance.addRow([your_relevance[i][0], null,your_relevance[i][1]]);
            sofarRelevance.addRow([your_relevance[i][0],null, your_relevance[i][1] / your_relevance[i][0] * 100]);
        }
        

        var chart = new google.visualization.LineChart(document.getElementById('chart_relevance'));
        chart.draw(pureRelevance, {
            'chartArea': {'left': 40, 'width': '75%', 'height': '80%'},
            hAxis: {titleTextStyle: {color: '#333'}},
            vAxis: {minValue: 0}
        });

        var chart2 = new google.visualization.LineChart(document.getElementById('chart_relevance_sofar'));
        chart2.draw(sofarRelevance, {
            'chartArea': {'left': 40, 'width': '75%', 'height': '80%'},
            hAxis: {titleTextStyle: {color: '#333'}},
            vAxis: {'title':'%', minValue: 0}
        });
        
        //diversity
        var pureDiversity = new google.visualization.DataTable();
        pureDiversity.addColumn('number', 'X');
        pureDiversity.addColumn('number', 'Original');
        pureDiversity.addColumn('number', 'Given');
        
        var normalDiversity = new google.visualization.DataTable();
        normalDiversity.addColumn('number', 'X');
        normalDiversity.addColumn('number', 'Original');
        normalDiversity.addColumn('number', 'Given');
        
        var allClusters=orig_diversity[orig_diversity.length-1][1];

        for (var i = 0; i < orig_diversity.length; i++) {
            pureDiversity.addRow([orig_diversity[i][0], orig_diversity[i][1], null]);
            normalDiversity.addRow([orig_diversity[i][0], orig_diversity[i][1]/allClusters*100, null]);
        }
        
        for (var i = 0; i < your_diversity.length; i++) {
            pureDiversity.addRow([your_diversity[i][0], null, your_diversity[i][1]]);
            normalDiversity.addRow([your_diversity[i][0], null, your_diversity[i][1]/allClusters*100]);
        }
        /*
         for (var i = 0; i < 500; i++) {
         data.addRow([Math.sin(i / 25), null, Math.cos(i / 10) * 0.5]);
         }*/

        var chart = new google.visualization.LineChart(document.getElementById('chart_diversity'));
        chart.draw(pureDiversity, {
            'chartArea': {'left': 40, 'width': '75%', 'height': '80%'},
            hAxis: {titleTextStyle: {color: '#333'}},
            vAxis: {minValue: 0}
        });
        
        var chart2 = new google.visualization.LineChart(document.getElementById('chart_diversitynormal'));
        chart2.draw(normalDiversity, {
            'chartArea': {'left': 40, 'width': '75%', 'height': '80%'},
            hAxis: {titleTextStyle: {color: '#333'}},
            vAxis: {'title':'%',minValue: 0}
        });
        
        //F1 chart
        var pureF1 = new google.visualization.DataTable();
        pureF1.addColumn('number', 'X');
        pureF1.addColumn('number', 'Original');
        pureF1.addColumn('number', 'Given');
        
        for (var i = 0; i < orig_f1.length; i++) {
            pureF1.addRow([orig_f1[i][0], orig_f1[i][1], null]);
        }
        
        for (var i = 0; i < your_f1.length; i++) {
            pureF1.addRow([your_f1[i][0], null,your_f1[i][1]]);
        }
        
        console.log(your_f1);
        
        var chart = new google.visualization.LineChart(document.getElementById('chart_f1'));
        chart.draw(pureF1, {
            'chartArea': {'left': 40, 'width': '75%', 'height': '80%'},
            hAxis: {titleTextStyle: {color: '#333'}},
            vAxis: {minValue: 0}
        });
    }
</script>