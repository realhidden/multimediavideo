<?php
if ($argc<2)
	die("Usage: php ytapi.php \"Fat search string\"\n");


$j=json_decode(file_get_contents("https://gdata.youtube.com/feeds/api/videos?q=".urlencode($argv[1])."&duration=short&v=2&alt=json"));

///make me a nice youtube download command
	/* 18	:	mp4	[360x640]
	-o, --output TEMPLATE      output filename template. Use %(title)s to get
                               the title, %(uploader)s for the uploader name,
                               %(uploader_id)s for the uploader nickname if
                               different, %(autonumber)s to get an automatically
                               incremented number, %(ext)s for the filename
                               extension, %(upload_date)s for the upload date
                               (YYYYMMDD), %(extractor)s for the provider
                               (youtube, metacafe, etc), %(id)s for the video id
                               , %(playlist)s for the playlist the video is in,
                               %(playlist_index)s for the position in the
                               playlist and %% for a literal percent. Use - to
                               output to stdout. Can also be used to download to
                               a different directory, for example with -o '/my/d
                               ownloads/%(uploader)s/%(title)s-%(id)s.%(ext)s' .
	*/

$nospace=str_replace(" ","",$argv[1]);
echo("mkdir \"$nospace\"\n");
echo("cd \"$nospace\"\n");
$i=1;
foreach ($j->feed->entry as $vi)
{
	//some magic
	$vi=get_object_vars($vi);
	$vi=get_object_vars($vi['media$group']);
	$vi=get_object_vars($vi['yt$videoid']);
	$ytid=$vi['$t'];
	//tada


      echo("youtube-dl -f 18 -o \"$nospace-".str_pad($i, 3, '0', STR_PAD_LEFT).".mp4\"");
	echo(" http://www.youtube.com/watch?v=$ytid\n");
      $i++;
}

echo("cd ..\n");
?>