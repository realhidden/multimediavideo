#!/bin/bash

if [ "$#" -lt "1" ]; then
	echo "Not enough parameter"
	echo "usage: $0 <path to video folder to be recursively searched>"
	exit -1
fi

for f in $(find $1 -type f -name "*.mp4"); do
	d=$(dirname "$f")
	b=$(basename "$f" .mp4)
	if [ -f $d/$b.txt ]; then
		./split_video.sh $f $d/$b.txt
	else
		echo "Warning! '$d/$b.txt' is missing."
	fi
done

