#!/bin/bash

datediff() {
    d1=$(date -d "$1" +%s.%N)
    d2=$(date -d "$2" +%s.%N)
    echo $(echo "$d1 - $d2" | bc -l)
}

if [ "$#" -lt "2" ]; then
	echo "Not enough parameter"
	echo "usage: $0 <video file> <txt file>"
	exit -1
fi

dirname=$(dirname "$1")
bname=$(basename "$1" .mp4)
mkdir -p $dirname/cuts/$bname
name=$dirname/cuts/$bname/$bname

len=$(ffmpeg -i "$1" 2>&1 | grep "Duration:" | cut -f2- -d: | cut -f1 -d, | tr -d ' ')
timelst=$(echo $(cat "$2") $len)

cnt=1
start=0
for i in $timelst; do
	duration=$(datediff $i $start)
	cutname=$(printf "%s-cut_%03d-%s-%s" "$name" "$cnt" "$start" "$i")
	cutname=$(echo $cutname | tr ":." "__")

	ffmpeg -y -i "$1" -ss $start -t $duration -strict experimental -vcodec libtheora -acodec vorbis "${cutname}.ogv"
	ffmpeg -y -i "$1" -ss $start "${cutname}.png"

	start=$i
	((cnt++))
done

