<?php

if (count($argv) < 2)
{
    die("Usage: php convert.php <locationname>\n");
}

$basedir = "../../dataset/metrics/".$argv[1]."/";
$rgt = "../../dataset/annotation/" . $argv[1] . ".rGT.csv";
$dgt = "../../dataset/annotation/" . $argv[1] . ".dGT.csv";
$outputcsv = "../../dataset/metrics/".$argv[1].".csv";

$files = array();

//load rGT, dGT if set
function loadTwovaluedCsv($filename) {
    if (!file_exists($filename))
        die("Unable to load: " . $filename);

    $ret = array();
    $lines = explode("\n", file_get_contents($filename));
    foreach ($lines as $l1) {
        $l1 = explode(" ", $l1);

        if ($l1[0] == "")
            continue;
        
        //fixing dgt / rgt 
        $tmp=explode("-",$l1[0]);
        $key=$tmp[0]."-".$tmp[1]."-".$tmp[2];
        $ret[$key] = $l1[1];
    }
    ksort($ret);
    return $ret;
}

$rGT = loadTwovaluedCsv($rgt);
$dGT = loadTwovaluedCsv($dgt);

//find all files
if ($handle = opendir($basedir)) {
    /* This is the correct way to loop over the directory. */
    while (false !== ($entry = readdir($handle))) {
        if ((!is_dir($basedir . $entry)) && ($entry != ".") && ($entry != "..") && ($entry != ".DS_Store")) {
            $files[] = $entry;
        }
    }
    closedir($handle);
}

//split filenames into cut + extension
$shots = array();
$extensions = array();

foreach ($files as $onefile) {
    $onefile = explode(".", $onefile, 3);

    if (count($onefile) < 3)
        continue;

    if (strtolower($onefile[2]) != "csv")
        continue;

    if (!in_array($onefile[0], $shots))
        $shots[] = $onefile[0];
    if (!in_array($onefile[1], $extensions))
        $extensions[] = $onefile[1];
}

sort($shots);
sort($extensions);

//well, start the crazyness
$fpcsv = fopen($outputcsv, 'w');

//for each shot
$lineid = 0;
foreach ($shots as $shot) {
    $row = array();
    $header = array();
    
    //first 3: shot name, rGt, dGt
    $row[] = $shot;
    $header[] = "shotname";
    
    $row[] = @$rGT[$shot];
    $header[] = "rgt";
    
    $row[] = @$dGT[$shot];
    $header[] = "dgt";

    //for each extension
    foreach ($extensions as $extension) {
        $data = str_getcsv(@file_get_contents($basedir . $shot . "." . $extension . ".csv"), ";");

        $extid = 0;
        foreach ($data as $item) {
            if (trim($item)=="")
                $item=0;
            
            $row[] = $item;

            //change delimiter
            $item = str_replace(".", ",", $item);

            //header
            if ($lineid == 0) {
                $header[] = $extension . "-" . $extid;
            }
            $extid++;
        }
    }

    //write header before first line
    if ($lineid == 0) {
        fputcsv($fpcsv, $header, ";");
    }
    fputcsv($fpcsv, $row,";");
    $lineid++;
}

fclose($fpcsv);
?>