#!/bin/bash

if [ "$#" -lt "1" ]; then
	echo "Not enough parameter"
	echo "usage: $0 <path to video folder to be recursively searched>"
	exit -1
fi

for f in $(find $1 -type f -name "*.mp4"); do
	echo $f
	python scene_extract.py $f
done

