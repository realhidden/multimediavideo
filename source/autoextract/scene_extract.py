from SimpleCV import *
import pylab, sys, os, datetime
import numpy as np
from pykalman import KalmanFilter
#import cv2
#import cv2.cv as cv
import ffvideo

CUT_THRESHOLD = 3.0
MIN_FRAMENUM = 6
SCALE = 0.3

class CutDetector:
	def __init__(self):
		self.meas = [];
		tr = np.eye(12)
		for i in range(6):
			tr[i,i+6] = 1
		ob = np.hstack((np.eye(6), np.zeros((6,6))))
		self.kf = KalmanFilter(transition_matrices = tr, observation_matrices = ob)
		
	def push_frame(self, feat):
		self.meas += [feat,]
		if len(self.meas) < MIN_FRAMENUM:
			return 0
		m = np.array(self.meas)
		flt, _ = self.kf.filter(m)
	
		d = np.linalg.norm(flt[-1,:6] - m[-1,:])
		
		if d > CUT_THRESHOLD:
			self.meas = []
		
		return d

def calc_feat(im):
	r,g,b = im.scale(SCALE).splitChannels()
	feat = []
	for c in [r,g,b]:
		nc = c.getGrayNumpy()
		feat += [nc.mean(), nc.std()]
	return np.array(feat)

def time_format(sec):
	return str(datetime.timedelta(seconds=sec))

def main():
	#pylab.ion()
	#pylab.cla()

	# sometimes it fails to get the real fps
	#vc = cv2.VideoCapture(sys.argv[1])
	#spf =  1/vc.get(cv.CV_CAP_PROP_FPS)
	
	vs = ffvideo.VideoStream(sys.argv[1])
	spf = 1/vs.framerate
	
	vc = VirtualCamera(sys.argv[1], "video")
	cd = CutDetector()
	fn = os.path.splitext(sys.argv[1])[0]

	with open(fn + ".txt", "w") as f:

		fr = vc.getImage()
		fcnt = 0
		while fr != None:
			feat = calc_feat(fr)
			d = cd.push_frame(feat)
			if d > CUT_THRESHOLD:
				t = time_format(fcnt * spf)
				print(t)
				f.write(t + "\n")
			
			#if d > CUT_THRESHOLD:
			#	pylab.scatter(fcnt, d, color="red")
			#else:
			#	pylab.scatter(fcnt, d, color="black")

			#pylab.draw()
			#fr.show()
	
			fr = vc.getImage()
			fcnt += 1

if __name__ == "__main__":
	#try:
	main()
	#exit(0)
	#except Exception, e:
	#	print(e.__doc__)
	#	print(e.message)
	#	exit(-1)

