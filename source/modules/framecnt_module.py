from base_module import BaseModule
import numpy as np
from SimpleCV import Image

class FrameCnt(BaseModule):
	
	def __init__(self):
		super(FrameCnt, self).__init__("frcnt", mode=BaseModule.EVERY_FRAME)

	def _begin_process(self):
		self.n = 0
		
	def _end_process(self):
		return np.array([self.n])

	def _process_frame(self, frame, framemeta):
		self.n += 1

