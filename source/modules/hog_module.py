from base_module import BaseModule
import numpy as np
from SimpleCV import Image
import scipy.ndimage.filters

class HOGModule(BaseModule):
	
	def __init__(self, bins=16):
		super(HOGModule, self).__init__("hog" + str(bins))
		self.bins = bins
	
	def _process_frame(self, frame, framemeta):
		
		im = Image(frame)
		w = im.width
		h = im.height
		floatim = im.getGrayNumpy().astype(float)

		# x and y deriv kernels
		kerns = [ [[0,0,0],[-1,0,1],[0,0,0]], [[0,-1,0],[0,0,0],[0,1,0]] ]
		derivs = [None, None]
		for idx, k in enumerate(kerns):
			derivs[idx] = scipy.ndimage.filters.convolve(floatim, np.array(k), mode='mirror')
			
		dx, dy = derivs
		angles = np.arctan2(dy, dx)
		
		hist, _ = np.histogram(angles, range=(-np.pi, np.pi), bins = self.bins)
		return 1/float(w * h) * hist
