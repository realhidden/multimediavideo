from base_module import BaseModule
import numpy as np
from SimpleCV import Image

class FrameDiff(BaseModule):
	
	def __init__(self):
		super(FrameDiff, self).__init__("frdiff", mode=BaseModule.EVERY_FRAME)
		self.lastframe = None
		self.n = 0
		self.sum = np.array([0,0,0])

	def _begin_process(self):
		self.lastframe = None
		self.n = 0
		self.sum = np.array([0,0,0])

	def _end_process(self):
		if self.n == 0:
			return np.array([-1])
		else:
			return self.sum / float(self.n)

	def _process_frame(self, frame, framemeta):
		if self.lastframe is None:
			self.lastframe = frame
			self.n = 0
		else:
			self.n += 1
			# TODO: handle image size mismatch
			sz = frame.shape[0] * frame.shape[1]
			dif = np.abs(frame - self.lastframe)
			self.sum += np.array([np.sum(dif[:,:,i]) / float(sz) for i in range(3)])
