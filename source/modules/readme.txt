To add a module you should do the following:
1) derive a class from BaseModule
2) register your module in main.py

See sobel_module.py for example.
