from base_module import BaseModule
import numpy as np
import scipy.stats

class ColorMomentsModule(BaseModule):
	
	def __init__(self):
		super(ColorMomentsModule, self).__init__("cm")

	# frame is a numpy ndarray, with the shape of (width, height, 3)
	# it is garanteed that the image has 3 channels
	# the result shuold be a numpy array with shape of (length,1)
	def _process_frame(self, frame, framemeta):
		
		im = frame
		r = im.shape[0]
		c = im.shape[1]
		s0 = scipy.stats.skew(im[:,:,0].reshape(r*c, 1))
		s1 = scipy.stats.skew(im[:,:,1].reshape(r*c, 1))
		s2 = scipy.stats.skew(im[:,:,2].reshape(r*c, 1))
		
		return np.array([im[:,:,0].mean(), im[:,:,0].std(), s0,
			im[:,:,1].mean(), im[:,:,1].std(), s1,
			im[:,:,2].mean(), im[:,:,2].std(), s2])

