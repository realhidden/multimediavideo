from base_module import BaseModule
import numpy as np
from SimpleCV import Image

class ColorHistModule(BaseModule):
	
	def __init__(self, name, bins=16, useHSV=False):
		super(ColorHistModule, self).__init__(name)
		self.useHSV = useHSV
		self.bins = bins

	def _process_frame(self, frame, framemeta):
		
		im = Image(frame)
		# normalizer factor
		fact = 1/float(im.width * im.height)
		
		if self.useHSV:
			im = im.toHSV()
		
		c0,c1,c2 = im.splitChannels()
		
		# join the 3 histograms and normalize
		return fact * np.array(c0.histogram(self.bins) + c1.histogram(self.bins) + c2.histogram(self.bins))

