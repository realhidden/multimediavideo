from base_module import BaseModule
from SimpleCV import *
import os

class FaceModule(BaseModule):
	
	def __init__(self):
		super(FaceModule, self).__init__("face")
		self.faces = HaarCascade(os.path.dirname(os.path.realpath(__file__))+"/haarcascade_frontalface_alt.xml")

	# Calculates the "area with faces" / "whole image" ratio
	def _process_frame(self, frame, framemeta):
		img = Image(frame)
		faces= img.findHaarFeatures(self.faces)
		if faces is None:
			return np.array([0])
		else:
			return np.array([float(sum(map(lambda x: x.area(),faces))) / (img.area()) ])

