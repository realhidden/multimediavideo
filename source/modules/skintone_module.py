from base_module import BaseModule
import numpy as np
from SimpleCV import Image

class SkintoneModule(BaseModule):
	
	def __init__(self):
		super(SkintoneModule, self).__init__("skintone")
		
	def _process_frame(self, frame, framemeta):
		
		im = Image(frame)		
		blobs = im.findSkintoneBlobs()
		if blobs != None and len(blobs) > 0:
			return np.array([blobs.area().sum() / im.width * im.height])
		else:
			return np.array([0,])

