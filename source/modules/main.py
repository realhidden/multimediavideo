import sys
from base_module import VideoProcessor

from sobel_module import SobelModule
from colormoments_module import ColorMomentsModule
from colorhist_module import ColorHistModule
from skintone_module import SkintoneModule
from hog_module import HOGModule
from face_module import FaceModule
from framediff_module import FrameDiff
from framecnt_module import FrameCnt

def main():

	if len(sys.argv) < 3:
		print("Usage: %s <mp4 file> <timestamp txt file>" % sys.argv[0])
		return -1

	vp = VideoProcessor(sys.argv[1], sys.argv[2])

	# append your module here
	modules = [
		ColorMomentsModule(),
		SobelModule(),
		ColorHistModule("rgbhist16", 16, False),
		ColorHistModule("hsvhist16", 16, True),
		SkintoneModule(),
		HOGModule(bins=16),
		FaceModule(),
		FrameDiff(),
		FrameCnt()
		]

	for m in modules:
		vp.register_frame_processor(m)
	
	vp.process()

if __name__ == "__main__":
	main()
