from base_module import BaseModule
import numpy as np
from SimpleCV import Image

class SobelModule(BaseModule):
	
	def __init__(self):
		super(SobelModule, self).__init__("sobel")

	# frame is a numpy ndarray, with the shape of (width, height, 3)
	# it is garanteed that the image has 3 channels
	# the result shuold be a numpy array with shape of (length,1)
	def _process_frame(self, frame, framemeta):
		
		im = Image(frame).sobel().getNumpy()
		
		return np.array([im[:,:,0].mean(), im[:,:,0].std(),
			im[:,:,1].mean(), im[:,:,1].std(),
			im[:,:,2].mean(), im[:,:,2].std()])

