import os, logging, ffvideo, re
from datetime import datetime
from SimpleCV import *
import numpy as np
import locale
import platform

class VideoProcessor(object):

	def __init__(self, videofile, timestampfile):
		self.modules = []
		self.load(videofile, timestampfile)
		
	def load(self, videofile, timestampfile):
		self.videofile = videofile
		self.timestampfile = timestampfile		
		self.video = None
		self.middles = None
		self.timestamps = []
		self._prepare()
		
	def _to_sec(self, string):
		fmtlist = ["%H:%M:%S.%f", "%H:%M:%S"]
		d = None
		for fmt in fmtlist:
			try:
				d = datetime.strptime(string, fmt)
			except:
				pass
		if d:
			sec = (d.hour*60+d.minute)*60+d.second+d.microsecond*0.000001
		return sec
	
	def _prepare(self):
		self.video = ffvideo.VideoStream(self.videofile)
		timestamps = [0.0]
		with open(self.timestampfile, "r") as f:
			for line in f:
				if line != "":
					timestamps.append(self._to_sec(line[:-1]))
		timestamps.append(self.video.duration)
	
		self.timestamps = timestamps
	
		self.middles = []
		for i in range(len(timestamps)-1):
			self.middles.append((timestamps[i+1] - timestamps[i])/2.0 + timestamps[i])
		
	def register_frame_processor(self, proc):
		"""
		proc - BaseModule derivative processor module
		mode - RegisterMode enum
		"""
		self.modules.append(proc)
		
	def process(self):
		if self.video is None or self.middles is None:
			logging.error(("Can not start processing because of some failure at"
				" preparation phase, see log above."))
		
		videoname = os.path.splitext(os.path.basename(self.videofile))[0]
		
		middle_mods = [m for m in self.modules if m.mode == BaseModule.MIDDLE_FRAME]
		every_mods = [m for m in self.modules if m.mode == BaseModule.EVERY_FRAME]
		
		self._process_middle_frame(middle_mods, videoname)
		self._process_every_frame(every_mods, videoname)

	def _ensure3channel(self, fr):
		if len(fr.shape) < 3:
			tmp = np.ndarray(fr.shape + (3,), fr.dtype)
			tmp[:,:,0] = fr
			tmp[:,:,1] = fr
			tmp[:,:,2] = fr
			return tmp
		else:
			return fr
			
	def _process_middle_frame(self, mods, vidname):
		
		for idx, s in enumerate(self.middles):
			fr = self.video.get_frame_at_sec(s).ndarray()
			fr = self._ensure3channel(fr)
			for m in mods:
				logging.info("Processing %s" % m.metricname)
				m.begin(idx, vidname)
				# TODO: fill frame meta
				fm = FrameMeta()
				m.process(fr, fm)
				m.end()
				
	def _process_every_frame(self, mods, vidname):

		for m in mods:
			logging.info("Processing %s" % m.metricname)
			idx = 0
			m.begin(idx, vidname)
			idx = 1
			for frame in self.video:
				if idx < len(self.timestamps) and frame.timestamp < self.timestamps[idx]:
					fm = FrameMeta()
					# TODO: fill frame meta
					fr = self._ensure3channel(frame.ndarray())
					m.process(fr, fm)
				else:
					m.end()
					idx += 1
					if idx < len(self.timestamps):
						m.begin(idx, vidname)

class FrameMeta(object):
	
	def __init__(self):
		self.framenum = -1
		self.frametime = 0

class BaseModule(object):
	
	MIDDLE_FRAME = -2
	EVERY_FRAME = 1
	
	def __init__(self, metricname, mode=MIDDLE_FRAME):
		self.metricname = metricname
		self.outname = ""
		self.resultvec = np.array([])
		self.mode = mode
		
	def _generate_output_name(self, videoname, framenum):
		return "%s-cut_%03d.%s.csv" % (videoname, framenum+1, self.metricname)
	
	def begin(self, index, videoname):
		self.outname = self._generate_output_name(videoname, index)
		self._begin_process()
		
	def _begin_process(self):
		pass
		
	def end(self):
		vec = self._end_process()
		self._write_frame_vector(self.outname, vec)
		
	def _end_process(self):
		return self.resultvec
	
	def process(self, frame, framemeta):
		self.resultvec = self._process_frame(frame, framemeta)
	
	def _process_frame(self, frame):
		raise Exception("BaseModule can not be called")
		
	def _write_frame_vector(self, outname, vec):
		# ensure '.' as the decimal delimiter
		if platform.system() == 'Darwin':
			locale.setlocale(locale.LC_ALL, "en_US.utf-8")    
		else:
			locale.setlocale(locale.LC_ALL, "en_US.utf8")
		with open(outname, "w") as f:
			# convert an ndarray to a list
			if len(vec.shape) > 0 and vec.shape[0] > 1:
				l = list(vec.reshape((reduce(lambda x,y: x*y, vec.shape), 1)).squeeze())
				f.write(";".join([str(x) for x in l]))
			else:
				f.write(str(vec[0]))
		# default locale
		locale.setlocale(locale.LC_ALL, '')

