<?php

$basedir = "../../dataset/yt/";

if (isset($_GET['all_loc'])) {
    $locs = array();

    if ($handle = opendir($basedir)) {
        /* This is the correct way to loop over the directory. */
        while (false !== ($entry = readdir($handle))) {
            if ((is_dir($basedir . $entry)) && ($entry != ".") && ($entry != "..")) {
                $locs[] = $entry;
            }
        }
        closedir($handle);
    }

    header('Content-type: application/json');
    sort($locs);
    echo(json_encode($locs));
    die();
}

if (isset($_GET['loc'])) {
    $files = array();

    $cutdir = $basedir . "/" . $_GET['loc'] . "/cuts/";
    if ($handle = opendir($cutdir)) {
        /* This is the correct way to loop over the directory. */
        while (false !== ($entry = readdir($handle))) {
            if ((is_dir($cutdir . $entry)) && ($entry != ".") && ($entry != "..")) {
                if ($handle2 = opendir($cutdir . $entry)) {
                    
                    while (false !== ($entry2 = readdir($handle2))) {
                        if (substr($entry2,-4)==".png")
                                $files[]=array(
                                    "id"=>substr($entry2,0,strlen($entry2)-4),
                                    "video"=>$_GET['loc'] . "/cuts/".$entry."/".substr($entry2,0,strlen($entry2)-4).'.ogv',
                                    "img"=>$_GET['loc'] . "/cuts/".$entry."/".substr($entry2,0,strlen($entry2)-4).'.png');
                    }
                    
                    closedir($handle2);
                }
            }
        }
        closedir($handle);
    }

    header('Content-type: application/json');
    sort($files);
    echo(json_encode($files));
    die();
}
?>
