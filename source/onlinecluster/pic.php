<?php
// Content type
header('Content-Type: image/jpeg');

//orig
$image = imagecreatefrompng("http://malna:malna@malna.tmit.bme.hu/mm/bitbucket/dataset/yt/".urldecode($_GET['p']));
$width = imagesx( $image );
$height = imagesy( $image );

// Get new dimensions
$new_width = $width; 
$new_height = $height;
if ($new_width>100)
{
    $new_height=$new_height*100/$new_width;
    $new_width=100;
}

if ($new_height>100)
{
    $new_width=$new_width*100/$new_height;
    $new_height=100;
}

// Resample
$image_p = imagecreatetruecolor($new_width, $new_height);
imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);

// Output
imagejpeg($image_p, null, 100);
?>