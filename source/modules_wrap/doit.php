<?php
$locs=array("BrandenburgGateinBerlin","EiffelTower","ForbiddenCity",
	"PlacedelaConcorde","SacreCoeur","SagradaFamilia","SummerPalace");
//"AcropolisofAthens","TheGreatWallofChina","TrafalgarSquare","

foreach ($locs as $loc1)
{
	echo("mkdir $loc1\ncd $loc1\n");
	for ($i=0;$i<25;$i++)
	{
		$ii=sprintf("%03d",$i+1);
		echo("echo \"$loc1-$ii\"\n");
		echo("python ../../../source/modules/main.py ../../yt/$loc1/$loc1-$ii.mp4 ../../yt/$loc1/$loc1-$ii.txt\n");
	}

	echo("php ../../../source/csvcompact/convert.php $loc1\n");
	echo("cd ..\n");
}
?>